FROM loftsh/go:latest

RUN apt-get install -y fortune

RUN chgrp -R 0 /app && \
    chmod -R g=u /app

ENV PATH=/usr/games:/go/bin:/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV HOME=/app
ENV GOPATH=/app/go
